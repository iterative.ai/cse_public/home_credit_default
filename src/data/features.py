from matplotlib.pyplot import axis
import pandas as pd
from pathlib import Path
from sklearn.preprocessing import OneHotEncoder
from typing import List, Text, Any

from src.utils.utils import rename_columns


def get_one_hot_encoder(df: pd.DataFrame, cat_features: List) -> Any:
    """Fit One Hot Encoder for list of categorical features.

    Args:
        df (pd.DataFrame): Data
        cat_features (List): List of categorical features

    Returns:
        Any: Encoder object
    """

    df_cat = df[cat_features]
    encoder = OneHotEncoder(sparse=False, handle_unknown='ignore')
    encoder.fit(df_cat)
    
    return encoder


def encode_cat_features(df: pd.DataFrame, cat_features: List[Text], encoder: Any, 
                        index_cols: List[Text] = None) -> pd.DataFrame:
    """Encode categorical features.

    Args:
        df (pd.DataFrame): Data
        cat_features (List[Text]): List of categorical features
        encoder (Any): Encoder object
        index_cols (List[Text], optional): List of index columns to apply from original Data. Defaults to None.

    Returns:
        pd.DataFrame: Encoded features data
    """    
    
    df_cat = df[cat_features]
    df_encoded = pd.DataFrame(encoder.transform(df_cat))
    df_encoded.columns = encoder.get_feature_names_out()
    
    if index_cols:
        features = pd.concat([df[index_cols], df_encoded], axis=1)
    else:
        features = df_encoded 
    
    return features


def get_aggregated_features(df: pd.DataFrame, feature_names: List[Text], agg_func: str,
                            index_cols: List[Text] = None) -> pd.DataFrame:
    """Aggregate features using aggregation function specified.

    Args:
        df (pd.DataFrame): Data
        feature_names (List[Text]): List of numerical features to apply
        agg_func (str): Aggregation function. Supported values: 'sum', 'mean'
        index_cols (List[Text], optional): Columns used to group & aggregate features. Defaults to None.

    Returns:
        pd.DataFrame: Aggregated features
    """    
    
    if agg_func == 'sum':
        agg_features = df.groupby(index_cols)[feature_names].sum()
        agg_features.columns = rename_columns(agg_features.columns, postfix='SUM')
  
    if agg_func == 'mean':
        agg_features = df.groupby(index_cols)[feature_names].mean()
        agg_features.columns = rename_columns(agg_features.columns, postfix='MEAN')  

    return agg_features 