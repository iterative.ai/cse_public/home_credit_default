import logging
import numpy as np
import pandas as pd
from sklearn.metrics import precision_score, recall_score
from typing import Iterable

from src.utils.logging import get_logger


def precision_at_k_score(actual: Iterable, predicted: Iterable, predicted_probas: Iterable, k: int) -> float:
    """Calculates Precision metric at top k (by prediction score value) objects.
    
    Args:
        actual {Iterable}: actual labels of the data
        predicted {Iterable}: predicted labels of the data
        predicted_probas {Iterable}: probability predictions for such data
        k {int}: top k value on which score will be calculated
    
    Returns:
        float: Precision@k value
    """

    df = pd.DataFrame({'actual': actual, 'predicted': predicted, 'probas': predicted_probas})
    df = df.sort_values(by=['probas'], ascending=False).reset_index(drop=True)
    df = df[:k]

    return precision_score(df['actual'], df['predicted'])


def recall_at_k_score(actual: Iterable, predicted: Iterable, predicted_probas: Iterable, k: int) -> float:
    """Calculates Recall metric at top k (by prediction score value) objects.
    
    Args:
        actual {Iterable}: actual labels of the data
        predicted {Iterable}: predicted labels of the data
        predicted_probas {Iterable}: probability predictions for such data
        k {int}: top k value on which score will be calculated
    
    Returns:
       float: Recall@k value
    """

    df = pd.DataFrame({'actual': actual, 'predicted': predicted, 'probas': predicted_probas})
    df = df.sort_values(by=['probas'], ascending=False).reset_index(drop=True)
    df = df[:k]

    return recall_score(df['actual'], df['predicted'])


def lift_score(actual: Iterable, predicted: Iterable, predicted_probas: Iterable, k: int) -> float:
    """Calculates Lift metric at top k (by prediction score value) objects.
    
    Args:
        actual {Iterable}: actual labels of the data
        predicted {Iterable}: predicted labels of the data
        predicted_probas {Iterable}: probability predictions for such data
        k {int}: top k value on which score will be calculated
    
    Returns:
        float: Lift@k value
    """

    logger = get_logger(__name__, logging.INFO)

    numerator = recall_at_k_score(actual, predicted, predicted_probas, k)
    denominator = np.mean(actual)

    lift = numerator / denominator
    logger.info(f'Lift: {numerator} / {denominator} = {lift}')

    return lift

