import logging
import numpy as np
import pandas as pd
from pandas.tseries.offsets import MonthBegin
from typing import Iterator, Tuple

from src.utils.logging import get_logger


def custom_ts_split(months: np.array, train_period: int = 1) -> Iterator:
    """Custom datetime splitter.
    
    Args:
        months {np.array}: months array
        train_period {int}: train period (months)
    
    Yields:
        tuple of start_train, end_train and test_period
    """

    logger = get_logger(__name__, logging.INFO)

    if len(months) == 0:
        logger.warning(f'train_period expected to be more or equal to 1')
        return

    if train_period < 1 or train_period >= len(months):
        raise ValueError(f'train_period is out of bounds 1-{len(months)}')

    for k, month in enumerate(months):

        start_train = pd.to_datetime(months.min())
        end_train = pd.to_datetime(start_train) + MonthBegin(train_period + k)
        test_period = pd.to_datetime(end_train + MonthBegin(1))

        if test_period <= pd.to_datetime(months.max()):
            yield start_train, end_train, test_period

        else:
            break


def get_split_data(features: pd.DataFrame, start_train: pd.Timestamp,
                   end_train: pd.Timestamp, test_period: pd.Timestamp
                   ) -> Tuple[pd.DataFrame, ...]:
    """Splits data for X_train, X_test, y_train, y_test.

    Args:
        features (pd.DataFrame): Dataset
        start_train (pd.Timestamp): Train period start date (month)
        end_train (pd.Timestamp): Train period emd date (month)
        test_period (pd.Timestamp): Test month

    Returns:
        Tuple[pd.DataFrame, ...]: Datasets X_train, X_test, y_train, y_test
    """    

    # Get train / test data for the split
    X_train = (features[(features['MONTH'] >= start_train) & (features['MONTH'] <= end_train)]
               .drop(columns=['SK_ID_CURR', 'MONTH', 'TARGET', 'DATE'], axis=1))
    X_test = (features[(features['MONTH'] == test_period)]
              .drop(columns=['SK_ID_CURR', 'MONTH', 'TARGET', 'DATE'], axis=1))
    y_train = features.loc[(features['MONTH'] >= start_train) & (features['MONTH'] <= end_train), 'TARGET']
    y_test = features.loc[(features['MONTH'] == test_period), 'TARGET']

    return X_train, X_test, y_train, y_test
