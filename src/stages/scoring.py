import argparse
from datetime import date
from dateutil.relativedelta import relativedelta
from mlem.api import apply
import pandas as pd
from pathlib import Path
from typing import Text

from src.stages.features_extract_bureau import calculate_bureau_features
from src.stages.features_extract_application import calculate_app_features
from src.utils.config import load_config
from src.utils.datetime_utils import check_isoformat
from src.utils.logging import get_logger


def scoring(config_path: Text, scoring_date: Text, storage_path: Text) -> None:
    """Calculate scores for a new data (with trained model).
    
    Args:
        config_path (Text): path to config file
        scoring_date (Text): date on which scoring is executed
    """

    config = load_config(config_path)
    logger = get_logger("SCORING", config.base.log_level)
    check_isoformat(scoring_date)
    logger.info(f"Scoring date: {scoring_date}")

    logger.info('Load datasets')
    
    if not storage_path:
        logger.warning(f'Storage path is not set. Default value will be used')
        storage_path = './data'

    storage_path = Path(storage_path)
    logger.info(f'Storage path = {storage_path}')
    dir_data_path = storage_path / config.data.scoring.dir_data_path
    bureau_data = pd.read_csv(dir_data_path / 'bureau.csv')
    app_data = pd.read_csv( dir_data_path / f'{scoring_date}.csv')

    logger.info('Build scoring dataset')
    bureau_features = calculate_bureau_features(bureau_data=bureau_data, config=config, logger=logger)
    app_features = calculate_app_features(app_data=app_data, config=config, logger=logger)
    scoring_data = pd.merge(app_features, bureau_features, how='left', on='SK_ID_CURR')
    scoring_data = scoring_data.drop(columns=['MONTH'], axis=1)   # 'SK_ID_CURR' is set as index!
    logger.info(f'Build scoring dataset -> Features shape: {scoring_data.shape}')

    # Fill NaNs: Random Forest estimator does not allow NaNs
    scoring_data.fillna(-1, inplace=True)

    logger.info('Scoring')
    preds = apply(config.train.mlem_model_path, scoring_data, method='predict')
    probas = apply(config.train.mlem_model_path, scoring_data, method='predict_proba')
    logger.info(f'Max probas: {probas[:, 1].max()}')

    logger.info('Save scores')
    scores_df = scoring_data.reset_index()[['SK_ID_CURR']].astype('int32')
    scores_df['SCORING_DATE'] = scoring_date
    scores_df['LABEL'] = preds.astype('int8')
    scores_df['SCORE'] = probas[:, 1]

    dir_scoring_results = storage_path / config.data.scoring.dir_results_path
    result_date = date.fromisoformat(scoring_date) + relativedelta(months=1, day=1)
    result_date = result_date.strftime('%Y-%m-%d')
    scoring_result_path = dir_scoring_results / f'{result_date}.csv'
    scores_df.to_csv(scoring_result_path, index=False)
    logger.info(f'Scores saved to: {scoring_result_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args_parser.add_argument('--scoring-date', dest='scoring_date', required=False, default=None)
    args_parser.add_argument('--storage-path', dest='storage_path', default=None)
    args = args_parser.parse_args()

    scoring(
        config_path=args.config,
        scoring_date=args.scoring_date,
        storage_path=args.storage_path
    )
