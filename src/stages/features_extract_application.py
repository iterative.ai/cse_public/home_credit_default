import argparse
import joblib
import pandas as pd
from pathlib import Path
from typing import Text, Any

from src.utils.config import load_config
from src.utils.logging import get_logger
from src.utils.utils import rename_columns
from src.data.features import get_one_hot_encoder, encode_cat_features


def calculate_app_features(app_data: pd.DataFrame, config: Any, logger: Any) -> pd.DataFrame:
    """Calculate Application data features.

    Args:
        app_data (pd.DataFrame): Application data
        config (Any): Config object (from params.yaml)
        logger (Any): Stage logger

    Returns:
        pd.DataFrame: Features with 'SK_ID_CURR' set as index
    """
    
    logger.info('Calculate features -> Load encoder')
    dir_models = Path(config.base.dir_models)
    encoder_path = dir_models / config.extract_features.application.encoder_path
    app_ohe = joblib.load(encoder_path)
    
    logger.info('Calculate features -> Encode Categorical features')
    app_cat_encoded = encode_cat_features(
        df=app_data, 
        cat_features=config.extract_features.application.cat_names,
        encoder=app_ohe,
        index_cols=['SK_ID_CURR']
    )
    app_cat_encoded = app_cat_encoded.set_index('SK_ID_CURR')

    logger.info('Calculate features -> Caclulate Num features ')
    app_data_num = (app_data
                    .drop(config.extract_features.application.cat_names, axis=1)
                    .set_index('SK_ID_CURR')
                    )
        
    logger.info('Calculate features -> Join features ')
    features = pd.concat([app_data_num, app_cat_encoded], axis=1)
    
    features.columns = rename_columns(features.columns, prefix='APP',
                                      exclude=['SK_ID_CURR', 'TARGET', 'MONTH', 'DATE'])
    logger.info(f'Calculate features -> Features shape: {features.shape}')
    
    return features


def features_extract_application(config_path: Text) -> None:
    """Calculate features for Application data.
    
    Args:
        config_path (Text): path to config file
    """

    config = load_config(config_path)
    logger = get_logger("FEATURES_application", config.base.log_level)
    dir_data_raw = Path(config.data.raw.dir_path)
    dir_data_processed = Path(config.data.processed.dir_path)
    dir_models = Path(config.base.dir_models)

    logger.info('Load dataset')
    df = pd.read_csv(dir_data_raw / config.data.raw.application_data_path)
    
    logger.info('Fit Onehot encoder')
    app_cat_features = config.extract_features.application.cat_names
    app_ohe = get_one_hot_encoder(df=df, cat_features=app_cat_features)
    encoder_path = dir_models / config.extract_features.application.encoder_path
    joblib.dump(app_ohe, encoder_path)
    logger.info(f'Save encoder to: {encoder_path}')
    
    logger.info('Calculate features')
    features = calculate_app_features(app_data=df, config=config, logger=logger)
    
    logger.info('Save features')
    features_path = dir_data_processed / config.extract_features.application.features_path
    features.to_csv(features_path)
    logger.info(f'Save processed features to: {features_path}')
    

if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    features_extract_application(config_path=args.config)
