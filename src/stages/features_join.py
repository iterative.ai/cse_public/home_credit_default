import argparse
import pandas as pd
from pathlib import Path
from typing import Text

from src.utils.config import load_config
from src.utils.logging import get_logger


def features_join(config_path: Text) -> None:
    """Load and process data.
    
    Args:
        config_path {Text}: path to config file
    """

    config = load_config(config_path)
    logger = get_logger("JOIN_FEATURES", config.base.log_level)
    dir_data_processed = Path(config.data.processed.dir_path)
    features_config = config.extract_features

    logger.info('Load data')
    bureau_data = pd.read_csv(dir_data_processed / features_config.bureau.features_path)
    app_data = pd.read_csv(dir_data_processed / features_config.application.features_path)

    logger.info('Join features')
    features = pd.merge(app_data, bureau_data, how='left', on='SK_ID_CURR')
    features.fillna(-1, inplace=True)

    logger.info('Save features')
    features_path = dir_data_processed / features_config.join_features.features_path
    features.to_csv(features_path, index=False)
    logger.info(f'Save processed features to: {features_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    features_join(config_path=args.config)
