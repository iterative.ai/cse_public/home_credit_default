from datetime import date
from dateutil.relativedelta import relativedelta
import re
from typing import Text


def first_day_of_month(dt: Text) -> Text:
    """Get first day of the month
    
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """

    d = date.fromisoformat(dt)
    last_day = d + relativedelta(day=1)

    return last_day.strftime('%Y-%m-%d')

# TODO: remove: unused
def prev_day(dt: Text) -> Text:
    """Get date of previous day
    
    Args:
        dt {Text}: date in ISO format (YYYY-MM-DD)
    Returns:
        Text: date in ISO format
    """
    return (date.fromisoformat(dt) + relativedelta(days=-1)).strftime('%Y-%m-%d')


def check_isoformat(date: Text) -> None:
    """Checks date type and format
    
    Args:
        date {Text}: date
    Raises:
        TypeError: if date type is not string
        ValueError: if date string is not in ISO format (YYYY-MM-DD)
    """

    if not isinstance(date, str):
        raise TypeError(f'Invalid date type: {type(date)}, must be string')

    if not re.match(r'^\d{4}-\d\d-\d\d', date):
        raise ValueError('Bad date format. ISO format required: YYYY-MM-DD')
    