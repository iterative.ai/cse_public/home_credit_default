Home Credit Default Risk
==============================

This tutorial is build on data for  Kaggle competition: [Home Credit Default Risk](https://www.kaggle.com/c/home-credit-default-risk/overview)

Original data was modified for educational purposes of the tutorial.

    Modifications made: 
        - add`date/time` dimensions to simulate real business problems
        - use Application and Bureau data only
        - split data for train & scoring stages
        - scoring data is randomly assigned to `monthly` buckets 


Repo structure:
---------------

```
.
├── .dvc                    <- DVC repository directory
├── config                  <- Configs directory (.env, .json, etc.)
├── dags                    <- Airflow DAGs
│   ├── __init__.py
│   ├── config.py
│   ├── scoring.py
│   └── utils
├── data
│ ├── processed             <- The final, canonical data sets for modeling
│ └── raw                   <- The original, immutable data dump
├── docker                  <- Dockerfile for an Airflow base images
│   └── airflow_base
├── models                  <- Trained and serialized models, model predictions, or model summaries
├── notebooks               <- Jupyter notebooks
├── reports                 <- Generated analysis as HTML, PDF, LaTeX, etc.
├── src                     <- Source code for use in this project.
│ ├── data                  <- Scripts to download, generate and transform data 
│ ├── evaluate              <- Evaluation metrics functions
│ ├── stages                <- DVC Pipeline stages to run 
│ └── utils                 <- Common used functions (logging, config loaders, etc.) 
├── tests                   <- Autotests 
├── dvc.lock                <- State of DVC pipeline
├── dvc.yaml                <- DVC DAG config
├── params.yaml             <- Pipeline config
├── README.md               <- The top-level README for developers using this project.
└── requirements.txt        <- Packages list

```

## Install & Setup DEV environment
DEV environment expects to be run on local (host) machine. Data Scientists work on this machine and run ML experiments (pipelines) via CLI in Terminal or Juputer Notebook.

**1. Prepare data storage**

To make the data available to all processes (Gitlab Runner, Airflow DAGs) we create a directory to store data. This data storage should be accessible by local experiments as well as for Airflow processes (see below).  

Add storage path to `config/.env`:

```dotenv
STORAGE_PATH=</absolute/path/to/data/storage>
```

example:

```dotenv
STORAGE_PATH=/home/user/storage
```

Under the `${STORAGE_PATH}` let's create a data storage structure:

```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p ${STORAGE_PATH}/{raw,scoring_data,true_labels,scoring_results}
```

*Notes:*
- By default we use `data` directory in the repo root for this purpose. However, in most real cases it's a dedicated location.
- In your projects you may move `STORAGE_PATH` configuration to `params.yaml`. 

**2. Download data**

Download data files [>>> from here <<<](https://drive.google.com/file/d/1Pa2UExaf7La8w2FY1DIQYOzO0930Okzl/view?usp=share_link)
Put (unzip) downloaded data to `${STORAGE_PATH}`

As a result you should have the following folder structure in our storage:
```
${STORAGE_PATH}
    ├── raw/                        <- raw data
    ├── scoring_data/               <- data used for scoring (by month)
    ├── scoring_results/            <- scoring results (by month)
    └── true_labels/                <- true labels
```

In the tutorial's pipeline config we specify paths like:
  ```yaml
  data:
    raw:
      dir_path: data/raw
      ...
    scoring:
      dir_data_path: scoring_data
      dir_results_path: scoring_results
      dir_true_labels_path: true_labels
  ```

**3. Build environment**

Create and activate virtual environment for `local` develomnet

```bash
python3 -m venv .venv
echo "export PYTHONPATH=$PWD" >> .venv/bin/activate
source .venv/bin/activate
pip install --upgrade pip setuptools wheel
pip install -r requirements.txt
```

**4. Add DVC remote storage (local)**
Add DVC remote path to `config/.env`:

```
DVC_STORAGE=/path/to/dvc/local/storage
```

Create directory which will be used as `DVC` remote (`local` remote `DVC` storage)

```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p ${DVC_STORAGE}
```

Add `DVC` remote:

```bash
dvc remote add --local -d local ${DVC_STORAGE}
```

**Note**: option `--local` saves remote configuration to the Git-ignored local config file


**5. Init MLEM**

```bash
mlem init
```

## Run ML experiments (pipelines) [DEV environment]

**1. Activate environment**

Activate virtual environment for `local` development

```bash
source .venv/bin/activate
export $(grep -v '#.*' config/.env | xargs)
```

Run Jupyter Notebooks (if needed)

```bash
jupyter notebook
```

Open [Jupyter Notebook](http://0.0.0.0:8888/notebooks/) (if needed).


**2. Run ML experiments and scoring pipeline**

```bash
dvc exp run
```

or create new one with NAME specified:

```bash
dvc exp run -n <NAME> [--set-param <param_name>=<param_value>]
```

**3. Commit Git & DVC changes for success experiment**

```bash
git add .
git commit -m "<Your experiment commit message>"
git push
dvc push
```

**4. Run (test) scoring pipeline**

```bash
python src/stages/scoring.py \
    --config=params.yaml \
    --scoring-date='2021-01-01' \
    --storage-path=${STORAGE_PATH}

python src/stages/scoring.py \
    --config=params.yaml \
    --scoring-date='2021-02-01' \
    --storage-path=${STORAGE_PATH}
```


## Run Airflow pipelines (PROD environment)

**1. Build PROD environment (Docker containers)**
```bash
export AIRFLOW_UID=$(id -u)
docker build \
  -t home_credit_base_airflow_docker_local \
  --build-arg AIRFLOW_UID=${AIRFLOW_UID} \
  -f docker/airflow_base/Dockerfile \
  .
```

**2. Setup Airflow Cluster**

Follow instructions in the [airflow_cluster](https://gitlab.com/iterative.ai/cse_public/airflow-cluster) repository to setup & run airflow services.

**3. Add the tutorial specific variables**

Create Airflow variables which are used in DAGs:

```dotenv
HOME_CREDIT_REPO_URL=<url_of_your_fork_fir_home_credit_default_repo>
HOME_CREDIT_BRANCH=<home_credit_default_branch>
HOME_CREDIT_REPO_USERNAME=<repo_user_name>
HOME_CREDIT_REPO_PASSWORD=<repo_password_or_token>
```

There are two ways to create variables in Airflow:
1\) via UI: **Admin** -> **Variables** you may add one by one or upload a `.json` file like one below. Don't forget to overwrite default values:
```json
  {
    "HOME_CREDIT_REPO_URL": "<url_of_your_fork_fir_home_credit_default_repo>",
    "HOME_CREDIT_BRANCH": "main",
    "HOME_CREDIT_REPO_USERNAME": "<repo_user_name>",
    "HOME_CREDIT_REPO_PASSWORD": "<repo_access_token>"
  }
```

2\) via terminal:
```bash
  # Enter container of airflow scheduler
  docker exec -ti airflow-scheduler /bin/bash
  
  # Add variable 
  airflow variables set <var_name> <value>
```


## Develop and run Airflow DAGs
For this tutorial purposes we have one Airflow DAG prepared:
- **scoring**: calculates scores (predictions) for customers based on new data

*Notes:*
- Airflow DAGs stored in the `dags/`directory
- `scoring` DAG require code from `src/`
- every run, a separate task in each DAG does clone the repository and switch to the specified branch (`main` by default)


**1. Deliver (copy) Airflow DAGs to AIRFLOW_HOME**

`AIRFLOW_HOME` is a root directory for the Airflow content. This is the default parent directory for Airflow assets such as DAGs and logs (more details in Airflow Docs)

Add path to Airflow home directory to `config/.env`
- AIRFLOW_HOME - absolute path to airflow home directory

*Example:*

```dotenv
# Airflow
AIRFLOW_HOME=/home/user/airflow-cluster/airflow
```

"Manual" delivery of DAGs from `dags/` to Airflow `$AIRFLOW_HOME/dags/` is just copying copying files like: 
 
```bash
export $(grep -v '#.*' config/.env | xargs)
mkdir -p $AIRFLOW_HOME/dags/home_credit_default
cp -r dags/* $AIRFLOW_HOME/dags/home_credit_default
```

**3. Run scoring simulation**

Workflow to run Airflow DAGs:
- run Airflow cluster (see above)
- enter [Airflow UI](http://localhost:8080)
- activate (unpause) DAG
- trigger DAG

There two options to unpause DAG `home_credit_default.scoring`.

1) from Airflow UI (toggle to unpause DAGs)
2) from CLI:

    ```bash
    docker exec -ti airflow-scheduler /bin/bash

    airflow dags unpause home_credit_default.scoring
    ```

## Setup CI/CD

To setup CI/CD pipeline you need setup an Airflow cluster in advance. During the CI/CD pipeline, Airflow DAGs from `dags/` folder are copied to `$AIRFLOW_HOME/dags/home_credit_default` directory. Additional configurations and environment variables are required.

**1. Create Personal Access Token in GitLab**
at GitLab user profile page -> **User Settings** -> Access Tokens


**2. Add Gitlab CI/CD Variables**

1. go to the page **Settings** -> **CI/CD** -> **Variables**;
2. click **Expand**;
3. add variables:

    ```dotenv
    AIRFLOW_HOME=<Airflow_home_directory> 
    repo_token=<repo_access_token>
    STORAGE_PATH=</absolute/path/to/data/storage>
    DVC_STORAGE=</absolute/path/to/dvc/remote/storage>
    ```

*Notes*:
- for `repo_token` flag *Masked* should be enabled
- to access variables from any branches, remove the **Protected** flag!


**3. Registration and launch of GitLab Runner**

Get the registration token:

1. go to the page **Settings** -> **CI / CD** -> **Specific Runners** -> **Set up a specific Runner manually**;
2. copy registration token from section `3.Use the following registration token during setup:`;
3. define environment variable:

    ```bash
    export $(grep -v '#.*' config/.env | xargs)
    export REGISTRATION_TOKEN=<registration_token>
    ```

Register gitlab runner (with docker executor)

```bash
gitlab-runner register \
            --non-interactive \
            -u https://gitlab.com/ \
            -r ${REGISTRATION_TOKEN} \
            --tag-list docker,home_credit \
            --executor docker \
            --docker-image ghcr.io/iterative/cml:0-dvc2-base1 \
            --docker-disable-cache \
            --docker-volumes "/tmp:/tmp" \
            --docker-volumes "${STORAGE_PATH}:${STORAGE_PATH}" \
            --docker-volumes "${DVC_STORAGE}:${DVC_STORAGE}" \
            --docker-volumes "${AIRFLOW_HOME}:${AIRFLOW_HOME}"
```

*Notes*:
* *--non-interactive* - allows to register runner in non-interactive mode
* *-u* - URL of CI server.


Launch gitlab runner:

```bash
gitlab-runner run 
```

**4. Run CI pipeline**

CI pipeline is configured in the file `.gitlab-ci.yml`.

The pipeline includes the following stages:

- *build*: build of project docker image
- *test*: run autotest
- *deploy_dags*: deploy (delivery) of DAGs to the host with Airflow
- *train*: train model, make report and create merge request using `cml`

Rules to run CI stages:
- *`build`* and *`test`*: on `Merge Request` to the `main` branch, sequentially
- *`train`*: on commit if the commit message contains `[exp]` string
- *`deploy_dags`*: on merge to the `main` branch


**To run `build`, `test` and `train` CI jobs:**
- enter the repository at GitLab
- create a new branch from the `main`branch and make any update
- create a `Merge Request` to the `main` branch
- or, make a commit to any branch with commit message containing '[exp]' string 

**To run `deploy_dags` CI job:**
- merge the request to `main` branch

Explanation for the `deploy_dags` CI job:

- the folder with the project name is created in directory `dags` inside Airflow home directory (${AIRFLOW_HOME}) 
- CI job `deploy_dags` does copy the content of the `dags/` directory in the repository to the project folder:
  ```
  export DAGS_FOLDER=${AIRFLOW_HOME}/dags/${PROJECT_FOLDER}

  # Create ${DAGS_FOLDER}
  rm -rf ${DAGS_FOLDER} && mkdir -p ${DAGS_FOLDER}

  # Copy content of folder ./dags to ${DAGS_FOLDER} directory
  cp -r dags/* ${DAGS_FOLDER}
  echo "Airflow DAGs copied to ${DAGS_FOLDER}"
  ```